Parse.initialize("OQAoLsG99ijEqPlPKCTRgh3g8tiI33j8xfsmOcu6", "V9eHbCwDymjdBK5tsd7D5UArBkYhuXqMLIBWh8m4");
 
var reports     = Parse.Object.extend("Reported_Items");
var users       = Parse.Object.extend("User");
var products    = Parse.Object.extend("Product");
var user        = null;
 
$("#login").submit( function (event) {
    event.preventDefault();

    var name = $("#username").val();
    var pass = $("#password").val();

    Parse.User.logIn( name, pass, {
        success : function (user) {
            location.hash = "#reports";
            user = Parse.User.current();
            var user_type = user.get("admin");
            if (user_type == true) {
                show_page(location.hash);
                get_reports();
            } else {
                logout('user');
            };
        },
        error   : function (user, error) {
            console.log("Login error " + error.message);
        }
    });
});
 
$("#logout").click( function (event) {
    logout();
});
 
$(window).on("hashchange", function (event) {
    if (Parse.User.current()) {
        if (location.hash == "#login") {
            show_page("#reports");
            get_reports();
        };
    } else {
        show_page("#login");
    }
});
 
function show_page(page) {
    $(".page").hide();
    $(page).show();
    location.hash = page;
};
 
function check_login () {
    if (Parse.User.current()) {
        user = Parse.User.current();
        location.hash = "#reports";
        show_page(location.hash);
        get_reports();
    } else {
        location.hash = "";
        location.hash = "#login";
    }
};

function logout (type) {
    if (type) {
        alert('Unauthorized User.');
    };
    Parse.User.logOut();
    var name = $("#username").removeAttr('value');
    var pass = $("#password").removeAttr('value');
    check_login();
};
 
function get_reports () {
    var query = new Parse.Query(reports);
        query.include("productId");
        query.include("productId.sellerId");
        query.descending("reportCount");
    query.find({
        success : function (results) {
            display_output(results);
        },
        error   : function (error) {
            console.log(error);
        }
    });
};
 
function display_output (results) {
    var output = "";
    for( var i in results) {
        var name            = results[i].get("productId");
            name            = name.get("sellerId");
            name            = name.get("username");
        var product         = results[i].get("productId");
            product         = product.get("name");
        var image           = results[i].get("productId");
            image           = image.get("productImage1");
            image           = image._url;
        var description     = results[i].get("productId");
            description     = description.get("description");
        var reports_count   = results[i].get("reportCount");
        var object_id		= results[i].id;
        output += "<tr>";
        output += "<td>" + name + "</td>";
        output += "<td>" + product + "</td>";
        output += "<td style='max-width:300px;'>" 
                + '<a href="'+image+'" target="_blank">' + image + '</a>' 
                + "</td>";
        output += "<td>" + description + "</td>";
        output += "<td>" + reports_count + "</td>";
        output += "<td>"
                + '<button data-id="'+object_id+'" type="button" style="margin-right:5px;" class="btn btn-xs btn-primary delete_user">Delete user</button>'
                + '<button data-id="'+object_id+'" type="button" style="margin-right:5px;" class="btn btn-xs btn-primary delete_product">Delete product</button>'
                + '<button data-id="'+object_id+'" type="button" style="margin-right:5px;" class="btn btn-xs btn-primary cancel_report">Cancel report</button>'
                + "</td>";
        output += "</tr>";
    };
    $("#reports-list").html(output);
    
    $(".delete_user").click(function (ev) {
        delete_user($(this).data('id'));
    });

    $(".delete_product").click(function (ev) {
        delete_product($(this).data('id'));
    });

    $(".cancel_report").click(function (ev) {
        cancel_report($(this).data('id'));
    });
};
 
function delete_user (value) {
    var events = 'user';
    destroy(value, events);
};
 
function delete_product (value) {
    var events = 'product';
    destroy(value, events);
};
 
function cancel_report (value) {
    var events = 'report';
    destroy(value, events);
};

function destroy (value, events) {
    var query = new Parse.Query(reports);
        query.include("productId");
        query.include("productId.sellerId");
    query.get(value, {
        success : function (result_obj) {
            if (events == 'user') {
                var user_id = result_obj.get("productId");
                    user_id = user_id.get("sellerId");
                    console.log(user_id.id);
                    user_delete(user_id.id);
                    result_obj.destroy({});
                get_reports();
            } else if (events == 'product') {
                var product_id = result_obj.get("productId");
                product_id.destroy({});
                result_obj.destroy({});
                get_reports();
            } else if (events == 'report') {
                result_obj.destroy({});
                get_reports();
            };
        },
        error   : function (error) {
            console.log(error);
        }
    });
};

function user_delete (id) {
    Parse.Cloud.run("deleteUser", { user_id:id }, {
        success : function (response) {
            console.log('response');
        },
        error   : function (error) {
            console.log(error);
        }
    });
};

setInterval(function() {
    check_login();
}, 90000);

check_login();