
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function (request, response) {
    response.success("Hello world!");
});

Parse.Cloud.define('deleteUser', function (request, response){                                                                                    
	Parse.Cloud.useMasterKey();
    var query = new Parse.Query("User");
    query.get(request.params.user_id, {
        success: function(user) {
            user.destroy({
                success: function() {
                    response.success('User deleted');
                },
                error: function(error) {
                    response.error(error);
                }
            });
        },
        error: function(error) {
            response.error(error);
        }
    });
});
